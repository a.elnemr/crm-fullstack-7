<?php
// Admins routes
Route::group(['middleware' => ['auth', 'Admin']], function () {
    Route::resource('/leads', 'LeadController');
    Route::post('/leads/assigned', 'LeadController@assigned')
        ->name('leads.assigned');
    Route::get('/leads/ajax/table', 'LeadController@table')->name('leads.table');

    Route::get('/calls/create/{lead}', 'CallController@create')
        ->name('calls.create');
    Route::post('/call/{lead}', 'CallController@store')
        ->name('calls.store');

    
});

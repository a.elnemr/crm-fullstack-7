@extends('layouts.app')

@section('title', 'Leads')

@section('pageHeader', 'All Leads')

@section('content')

    <div class="text-right p-3">
        @if (Auth::user()->role->slug == 'ADM')
        <!-- <a href="{{ route('leads.create') }}" class="btn btn-success">ADD NEW</a> -->
        <button type="button"
                    class="btn btn-success"
                    data-toggle="modal"
                    data-target="#modal-new-lead">
                ADD NEW
            </button>
        @endif
    </div>
   <div id="lead-table-content">
    {!! $table !!}
   </div>


    <!-- Modals -->
    <div class="modal fade" id="modal-assigned" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Assigned Sales Man</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('leads.assigned') }}"
                      method="post"
                      class="text-center">
                    @csrf
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-new-lead" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="form-error">
                </div>

                <form action="{{ route('leads.store') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        @include('shared.input', [
                                'name'=> 'name',
                                'label'=> 'Name',
                                'value'=> old('name')
                                ])
                        @include('shared.input', [
                                'name'=> 'phone',
                                'label'=> 'Phone',
                                'value'=> old('phone'),
                                'type'=> 'tel'
                                ])
                        @include('shared.input', [
                                'name'=> 'email',
                                'label'=> 'Email',
                                'value'=> old('email'),
                                'type'=> 'email'
                                ])

                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" id="address" value="{{ old('address') }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
